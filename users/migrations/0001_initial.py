# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='user',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=15)),
                ('user_name', models.CharField(max_length=15)),
                ('password', models.CharField(max_length=40)),
                ('email', models.EmailField(blank=True, max_length=254)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
    ]
