from __future__ import unicode_literals

from django.db import models

class user(models.Model):
    name = models.CharField(max_length=15)
    user_name = models.CharField(max_length=15)
    password = models.CharField(max_length=40)
    email = models.EmailField(blank=True)
    class Meta:
        ordering=['name']

    def __str__(self):
        return 'name:{} user_name:{}' .format(self.name , self.user_name)
