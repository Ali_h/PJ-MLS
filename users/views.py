from django.shortcuts import render , render_to_response , get_object_or_404 ,get_list_or_404
from django.urls import reverse
from django.http import HttpResponse , Http404 , HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from users.models import *
from books.models import *
from validate_email import validate_email #for validate email

def athenticate(username,password):	#This function checks the correction of username and password
	User = user.objects.filter(user_name=username , password=password)
	if len(User) == 1:
		return username
	elif len(User) == 0:
		return False

@csrf_exempt
def login(request):
    if request.method == "POST":
        errors=[]
        if 'user_name' not in request.POST or len(request.POST['user_name'])==0 :
            errors.append('Enter The User Name !!')
            return render_to_response('login.html' , {'errors':errors})
        elif 'password' not in request.POST or len(request.POST['password'])==0 :
            errors.append('Enter The Password !!')
            return render_to_response('login.html' , {'errors':errors})
        elif 'user_name' in request.POST and 'password' in request.POST :
            user_name = request.POST['user_name']
            password = request.POST['password']
            is_login = athenticate(user_name,password)
            if is_login == False:
                errors.append('User Name or Password Is Incorrect !!')
                return render_to_response('login.html' , {'errors':errors})
            else:
                request.session['is_login'] = True
                request.session['Username'] = user_name
                return HttpResponseRedirect('/home')
    else:
        if request.session['is_login']:
            return HttpResponseRedirect('/home')
        else:
            return render_to_response('login.html')

def logout(request):
    try:
        del request.session['Username']
        del request.session['is_login']
    except KeyError:
        pass
    return HttpResponseRedirect(reverse('users:login'))

@csrf_exempt
def register(request):
    errors = []
    register=False
    if request.method == 'POST':
        if 'name' not in request.POST or len(request.POST['name'])==0 :
            errors.append('Enter The Name !!')
            return render_to_response('register.html' , {'errors':errors })
        elif 'user_name' not in request.POST or len(request.POST['user_name'])==0 :
            errors.append('Enter The User Name !!')
            return render_to_response('register.html' , {'errors':errors})
        elif 'password' not in request.POST or len(request.POST['password'])==0 :
            errors.append('Enter The Password !!')
            return render_to_response('register.html' , {'errors':errors})
        elif 'email' in request.POST and len(request.POST['email'])!=0 and validate_email(request.POST['email'])==False:
            errors.append('Uncorrect Email !!')
            return render_to_response('register.html' , {'errors':errors})
        elif 'name' in request.POST and 'user_name' in request.POST and 'password' in request.POST:
            name=request.POST['name']
            user_name=request.POST['user_name']
            password=request.POST['password']
            email=request.POST['email']
            validate_user_name = user.objects.filter(user_name=user_name)
            if len(validate_user_name) != 0 :
                errors.append('In Nam Ghablan Boode Ast !!')
                return render_to_response('register.html' , {'errors':errors})
            elif len(password) <= 6 :
                errors.append('Password Is Too Short (bigger than 6 char) !!')
                return render_to_response('register.html' , {'errors':errors})
            else:
                users = user.objects.create(name=name , user_name=user_name , password=password , email=email)
                register=True
                return render_to_response('register.html' , {'register':register})
    else :
        return render_to_response('register.html')
