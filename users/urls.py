from django.conf.urls import url
from django.contrib import admin
from users.views import *

app_name = 'users' #baraye inke vaghti ye app dige ham yek name be esme detail dashte bashe ghati nakone
urlpatterns = [
    url(r'^register/$', register , name="register"),
    url(r'^login/$', login , name="login"),
    url(r'^logout/$', logout , name="logout"),
]
