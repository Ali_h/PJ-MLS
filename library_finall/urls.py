from django.conf.urls import url , include
from django.contrib import admin
from users.views import *
from books.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^user/', include('users.urls')),
    url(r'^home/$', home , name="home"),
]
