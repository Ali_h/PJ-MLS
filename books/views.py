from django.shortcuts import render , render_to_response
from django.http import HttpResponse , Http404 , HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from users.models import *
from books.models import *

@csrf_exempt
def home(request):
    if request.method == "POST":
        if request.POST['action']=='add_book':
            if 'isbn' not in request.POST or len(request.POST['isbn'])== 0 :
                return HttpResponse('Enter The Isbn!!')
            elif 'title' not in request.POST or len(request.POST['title'])== 0 :
                return HttpResponse('Enter The Title!!')
            elif 'author' not in request.POST or len(request.POST['author'])== 0 :
                return HttpResponse('Enter The Author!!')
            elif 'publisher' not in request.POST or len(request.POST['publisher'])== 0 :
                return HttpResponse('Enter The Publisher!!')
            elif 'isbn' in request.POST and 'title' in request.POST and 'author' in request.POST and 'publisher' in request.POST:
                isbn=request.POST['isbn']
                title=request.POST['title']
                publisher=request.POST['publisher']
                author=request.POST['author']
                validate_isbn = Book.objects.filter(isbn=isbn)
                validate_publisher = Publisher.objects.filter(name=publisher) #baraye field haye foriegnkey va many to many bayad ingoone amal kard ke az khode class esh bekhoonim va hamoon row ro bedim be create
                validate_author = Author.objects.filter(name=author)
                if len(validate_isbn) != 0 :
                    return HttpResponse('In Isbn Mojood Mibashad!!')
                elif len(validate_author) == 0:
                    return HttpResponse("In Author Mojood Nemibashad")
                elif len(validate_publisher) == 0:
                    return HttpResponse("In Publisher Mojood Nemibashad")
                else:
                    books = Book.objects.create(isbn=isbn , title=title , publisher=validate_publisher[0] , authors = validate_author[0])
                    return HttpResponse('Success !!')

        elif request.POST['action']=='add_publisher':
            if 'name' not in request.POST or len(request.POST['name'])== 0 :
                return HttpResponse('Enter The Name!!')
            else:
                name=request.POST['name']
                validate_name_publisher=Publisher.objects.filter(name=name)
                if len(validate_name_publisher) != 0 :
                    return HttpResponse("In Publisher Mojood Ast")
                else:
                    name_publisher = Publisher.objects.create(name=name)
                    return HttpResponse('Publisher Added Successfully !!')

        elif request.POST['action'] == "add_author":
            if 'name' not in request.POST or len(request.POST['name'])== 0 :
                return HttpResponse('Enter The Name!!')
            else:
                name=request.POST['name']
                validate_name_author=Author.objects.filter(name=name)
                if len(validate_name_author) != 0 :
                    return HttpResponse("In Author Mojood Ast")
                else:
                    name_publisher = Author.objects.create(name=name)
                    return HttpResponse('Author Added Successfully !!')
    else:
        if request.session["is_login"]:
            return render_to_response('home.html')
        else:
            return HttpResponse("avval login shavid")
