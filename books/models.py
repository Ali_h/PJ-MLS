from __future__ import unicode_literals
from django.db import models

class Publisher(models.Model):
    name = models.CharField(max_length=20)
    class Meta:
        ordering=['name']
    def __str__(self):
        return 'name:%s' %(self.name)

class Author(models.Model):
    name = models.CharField(max_length=20)
    class Meta:
        ordering=['name']
    def __str__(self):
        return 'name:%s' %(self.name)

class Book(models.Model):
    isbn = models.CharField(max_length=20)
    title = models.CharField(max_length=20)
    authors = models.ForeignKey(Author, on_delete=models.CASCADE)
    publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE)
    class Meta:
        ordering=['isbn']
    def __str__(self):
        return 'isbn:%s title:%s' %(self.isbn ,self.title)
